package com.lind.bookshop.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user-service")
public interface UserClient {
  @GetMapping("v1/api/users/{id}")
  ResponseEntity<?> getUser(@PathVariable("id") Long id);
}
